import 'package:flutter/material.dart';

import 'src/ui/widgets/navigation.dart';

//********************************* CLASS MAIN ************************************** */
void main() => runApp(MyApp());

//********************************** CLASS MY APP *********************************** */
class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
//*********************************************************************************** */