import 'package:flutter/material.dart';
import 'package:navigation_cuberto_bottom/src/ui/designer/cuberto_bottom_bar.dart';

//**************************************** CLASS NAVIGATION ********************************************* */
class MyHomePage extends StatefulWidget {
  //Variable
  final String title;

  //***************************** CONSTRUCT *********************** */
  MyHomePage({Key key, this.title}) : super(key: key);

  //***************************** CALL STATE ********************** */
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

//***************************************** STATE NAVIGATION ******************************************** */
class _MyHomePageState extends State<MyHomePage> {
  //Variable
  int currentPage = 0;
  String currentTitle = "Home";
  
  Color currentColor = Colors.deepPurple;
  Color inactiveColor = Colors.black;

  //*************************** WIDGET ROOT *********************** */
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(

      //:::::::::::::::::::::::::::: PAGE :::::::::::::::::::::::
      body: Container(
        decoration: BoxDecoration(color: currentColor),
        child: Center(
            child: Text(
          currentTitle,
          style: TextStyle(color: Colors.white, fontSize: 20.0),
        )),
      ),
      
      //:::::::::::::::::::::::::::: DRAWER START ::::::::::::::::
      drawer: new Container(
          width: 250.0,
          margin: EdgeInsets.only(bottom: 60.0),
          color: Colors.blue,
          child: ListView(
            children: <Widget>[Text("Hello"), Text("World")],
          )),
      
      //:::::::::::::::::::::::::::: DRAWER END :::::::::::::::::::
      endDrawer: new Container(
          width: 250.0,
          margin: EdgeInsets.only(bottom: 60.0),
          color: Colors.blue,
          child: ListView(
            children: <Widget>[Text("Hello"), Text("World")],
          )),
      
      //:::::::::::::::::::::::: BOTTOM NAVIGATION BAR ::::::::::::
      bottomNavigationBar: CubertoBottomBar(
        inactiveIconColor: inactiveColor,
        tabStyle: CubertoTabStyle.STYLE_FADED_BACKGROUND,
        initialSelection: 0,
        drawer: CubertoDrawer(style: CubertoDrawerStyle.START_DRAWER, icon: Icon(Icons.sort)),
        tabs: [
          TabData(
              iconData: Icons.home, 
              title: "Home", 
              tabColor: Colors.deepPurple),
          TabData(
              iconData: Icons.search, 
              title: "Search", 
              tabColor: Colors.pink),
          TabData(
              iconData: Icons.access_alarm,
              title: "Alarm",
              tabColor: Colors.amber),
          TabData(
              iconData: Icons.settings,
              title: "Settings",
              tabColor: Colors.teal),
        ],
        onTabChangedListener: (position, title, color) {
          setState(() {
            currentPage = position;
            currentTitle = title;
            currentColor = color;
          });
        },
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
//*********************************************************************************************************** */
